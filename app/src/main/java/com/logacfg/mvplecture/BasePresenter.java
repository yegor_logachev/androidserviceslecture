package com.logacfg.mvplecture;

/**
 * Created by Yegor on 7/29/17.
 */

public interface BasePresenter {
    void start();
}
