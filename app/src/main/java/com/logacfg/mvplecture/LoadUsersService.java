package com.logacfg.mvplecture;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.logacfg.mvplecture.data_source.DataSource;
import com.logacfg.mvplecture.data_source.DataSourceFabric;

/**
 * Created by Yegor on 9/2/17.
 */

public class LoadUsersService extends Service {

    public static final String USERS_LIST_EXTRA = "users_list_extra";

    private static final String LOG_TAG = "LoadUsersServiceTag";

    private DataSource dataSource;

    @Override
    public void onCreate() {
        super.onCreate();
        dataSource = DataSourceFabric.getInstance(this);
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        return START_NOT_STICKY;
    }

    public void loadUsers(DataSource.Callback callback) {
        dataSource.loadUsers(callback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return new MyBinder();
    }

    public class MyBinder extends Binder {

        public LoadUsersService getService() {
            return LoadUsersService.this;
        }
    }
}
