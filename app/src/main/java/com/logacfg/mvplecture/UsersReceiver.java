package com.logacfg.mvplecture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;

/**
 * Created by Yegor on 9/2/17.
 */

public class UsersReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ArrayList<User> parcelableArrayListExtra = intent.getParcelableArrayListExtra(LoadUsersService.USERS_LIST_EXTRA);
        Toast.makeText(context, "Data has been received size: " + parcelableArrayListExtra.size(), Toast.LENGTH_SHORT).show();
    }
}
